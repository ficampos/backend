package com.fabianocampos.fidbackapi.services;

import com.fabianocampos.fidbackapi.domain.*;
import com.fabianocampos.fidbackapi.domain.enums.UserType;
import com.fabianocampos.fidbackapi.dto.ProjectDTO;
import com.fabianocampos.fidbackapi.dto.UserProjectDTO;
import com.fabianocampos.fidbackapi.dto.converter.ProjectConverter;
import com.fabianocampos.fidbackapi.dto.converter.UserProjectConverter;
import com.fabianocampos.fidbackapi.dto.converter.enums.Operation;
import com.fabianocampos.fidbackapi.repository.LabelRepository;
import com.fabianocampos.fidbackapi.repository.ProjectRepository;
import com.fabianocampos.fidbackapi.repository.UserProjectRepository;
import com.fabianocampos.fidbackapi.services.exception.ObjectAlreadyExistsException;
import com.fabianocampos.fidbackapi.services.exception.ObjectNotFoundException;
import com.fabianocampos.fidbackapi.services.exception.PermissionInvalidException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProjectService {

    @Autowired
    private ProjectRepository repo;

    @Autowired
    private UserProjectRepository userProjectRepository;

    @Autowired
    private ProjectConverter projectConverter;

    @Autowired
    private UserService userService;

    @Autowired
    private LabelRepository labelRepository;

    @Autowired
    private UserProjectConverter userProjectConverter;

    @Autowired
    private UserProjectService userProjectService;

    @Autowired
    private ReportService reportService;

    public List<Project> findAll() {
        return repo.findAll();
    }

    public Project findById(Integer id, String connectedUserEmail) {
        User user = userService.findByEmail(connectedUserEmail);
        UserProject userProject = userProjectRepository.findUserProjectById(user.getId(), id);
        Project project = repo.findById(id).orElseThrow(() -> new ObjectNotFoundException("Projeto não encontrado!"));
        if (userProject == null && !project.isVisibility()) {
            throw new PermissionInvalidException("Usuário não tem permissão esta ação.");
        }
        return project;
    }

    public Project findById(Integer id) {
        return repo.findById(id).orElseThrow(() -> new ObjectNotFoundException("Projeto não encontrado!"));
    }

    public List<Project> findProjectsByUserId(Principal principal) {
        User user = userService.findByEmail(principal.getName());
        return repo.findProjectsByUserId(user.getId());
    }

    public List<Project> findByName(String nome, String connectedUserEmail) {
        User user = userService.findByEmail(connectedUserEmail);
        List<Project> projects = repo.findByName(nome);
        List<Project> newProjects = projects.stream().filter(project -> userProjectRepository.findUserProjectById(user.getId(), project.getId()) != null || project.isVisibility()
        ).collect(Collectors.toList());

        if (newProjects.isEmpty()) {
            throw new ObjectNotFoundException("Projeto não encontrado!");
        }

        return newProjects;
    }

    public Project createOrUpdate(ProjectDTO projectDTO, Operation operation, String connectedUserEmail) {
        User user = userService.findByEmail(connectedUserEmail);
        Project proj = repo.findProjectByNameAndByUserId(user.getId(), projectDTO.getName());
        UserProject userProjectAdmin = userProjectRepository.findUserProjectById(user.getId(), projectDTO.getId());

        if ((operation == Operation.CREATE && proj != null) || (operation == Operation.UPDATE && proj != null && proj.getId() != projectDTO.getId())) {
            throw new ObjectAlreadyExistsException("Já existe projeto com este nome");
        } else if ((userProjectAdmin == null || userProjectAdmin != null && !userProjectAdmin.getUserType().equals(UserType.ADMIN)) && operation.equals(Operation.UPDATE)) {
            throw new PermissionInvalidException("Usuário não tem permissão para esta ação");
        }

        Project project = projectConverter.decode(projectDTO, operation);
        project = repo.save(project);

        if (operation.equals(Operation.CREATE)) {
            UserProject userProject = UserProject.builder().id(UserProjectPK.builder().project(project).user(user).build()).userType(UserType.ADMIN).createdAt(new Date()).build();
            userProjectRepository.save(userProject);

            Label openLabel = Label.builder().title("Aberto").project(project).build();
            Label closeLabel = Label.builder().title("Fechado").project(project).build();

            labelRepository.save(openLabel);
            labelRepository.save(closeLabel);
        }

        return project;
    }

    public void delete(Integer id, String connectedUserEmail) {
        validateAdminType(connectedUserEmail, id, false);
        Project project = this.findById(id, connectedUserEmail);

        repo.deleteAllCategoriesByProjectId(id);
        repo.deleteAllLabelsByProjectId(id);
        repo.deleteAllParticipantsByProjectId(id);
        repo.deleteAllReportsByProjectId(id);
        repo.deleteAllTagsByProjectId(id);

        repo.delete(project);

    }

    public void newParticipant(Integer projectId, UserProjectDTO userProjectDTO, String connectedUserEmail) {
        validateAdminType(connectedUserEmail, projectId, true);
        UserProject userProject = userProjectRepository.findUserProjectById(userProjectDTO.getUserId(), projectId);
        if (userProject != null) {
            throw new ObjectAlreadyExistsException("Usuário já faz parte deste projeto.");
        }

        userProjectDTO.setProjectId(projectId);
        UserProject newUserProject = userProjectRepository.save(userProjectConverter.decode(userProjectDTO, Operation.CREATE));
        Project project = findById(projectId);
        project.getParticipants().add(newUserProject);
        repo.save(project);
    }

    public void changeParticipant(Integer projectId, UserProjectDTO userProjectDTO, String connectedUserEmail) {
        validateAdminType(connectedUserEmail, projectId, false);

        userProjectDTO.setProjectId(projectId);
        userProjectRepository.save(userProjectConverter.decode(userProjectDTO, Operation.UPDATE));
    }

    public List<User> findParticipants(Integer projectId, String connectedUserEmail) {
        connectedUserHasPermission(projectId, connectedUserEmail);

        List<User> participants = repo.findParticipantsByProject(projectId);
        return participants;
    }

    public User findParticipantById(Integer projectId, Integer userId, String connectedUserEmail) {
        return repo.findParticipantById(projectId, userId);
    }

    public void deleteParticipantById(Integer projectId, Integer userId, String connectedUserEmail) {
        validateAdminType(connectedUserEmail, projectId, false);

        userProjectRepository.delete(userProjectService.findById(userId, projectId));
    }

    private void validateAdminType(String connectedUserEmail, Integer projectId, boolean testOpenProject) {
        User user = userService.findByEmail(connectedUserEmail);
        UserProject userProject = userProjectRepository.findUserProjectById(user.getId(), projectId);
        Project project = findById(projectId);

        if ((userProject == null || !userProject.getUserType().equals(UserType.ADMIN)) && (testOpenProject ? !project.isVisibility() : true)) {
            throw new PermissionInvalidException("Usuário não tem permissão para esta ação.");
        }
    }

    public List<Tag> findTags(Integer projectId, String connectedUserEmail) {
        User user = userService.findByEmail(connectedUserEmail);
        UserProject userProject = userProjectRepository.findUserProjectById(user.getId(), projectId);

        if ((userProject == null || userProject.getUserType().equals(UserType.USER))) {
            throw new PermissionInvalidException("Usuário não tem permissão para esta ação.");
        }

        return repo.findTags(projectId);

    }

    public List<Report> findReports(Integer projectId, String connectedUserEmail) {
        connectedUserHasPermission(projectId, connectedUserEmail);

        return repo.findReports(projectId);
    }

    public void connectedUserHasPermission(Integer projectId, String connectedUserEmail) {
        User user = userService.findByEmail(connectedUserEmail);
        UserProject userProject = userProjectRepository.findUserProjectById(user.getId(), projectId);
        Project project = findById(projectId);

        if (!project.isVisibility() && userProject == null) {
            throw new PermissionInvalidException("Usuário não tem permissão para esta ação.");
        }
    }

    public void connectedUserHasDevPermission(Integer projectId, String connectedUserEmail) {
        User user = userService.findByEmail(connectedUserEmail);
        UserProject userProject = userProjectRepository.findUserProjectById(user.getId(), projectId);

        if (userProject == null || userProject.getUserType().equals(UserType.USER)) {
            throw new PermissionInvalidException("Usuário não tem permissão para esta ação.");
        }
    }
}