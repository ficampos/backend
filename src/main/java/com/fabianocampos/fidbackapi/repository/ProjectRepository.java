package com.fabianocampos.fidbackapi.repository;

import com.fabianocampos.fidbackapi.domain.Project;
import com.fabianocampos.fidbackapi.domain.Report;
import com.fabianocampos.fidbackapi.domain.Tag;
import com.fabianocampos.fidbackapi.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Integer> {

    List<Project> findByName(String name);

    @Query(value = "select p.* from project p inner join user_project up on p.id = up.project_id inner join user u on u.id = up.user_id where u.id = ?1", nativeQuery = true)
    List<Project> findProjectsByUserId(Integer id);

    @Query(value = "select p.* from project p inner join user_project up on p.id = up.project_id inner join user u on u.id = up.user_id where u.id = ?1 and p.name = ?2", nativeQuery = true)
    Project findProjectByNameAndByUserId(Integer id, String name);

    @Query(value = "select u from User u inner join UserProject up on up.id.user.id = u.id where up.id.project.id = ?1")
    List<User> findParticipantsByProject(Integer projectId);

    @Query(value = "select u from User u inner join UserProject up on up.id.user.id = u.id where up.id.project.id = ?1 and up.id.user.id = ?2")
    User findParticipantById(Integer projectId, Integer userId);

    @Query(value = "delete from user_project where project_id = ?1", nativeQuery = true)
    void deleteAllParticipantsByProjectId(Integer projectId);

    @Query(value = "delete from report where project_id = ?1", nativeQuery = true)
    void deleteAllReportsByProjectId(Integer projectId);

    @Query(value = "delete from category where project_id = ?1", nativeQuery = true)
    void deleteAllCategoriesByProjectId(Integer projectId);

    @Query(value = "delete from tag where project_id = ?1", nativeQuery = true)
    void deleteAllTagsByProjectId(Integer projectId);

    @Query(value = "delete from label where project_id = ?1", nativeQuery = true)
    void deleteAllLabelsByProjectId(Integer projectId);

    @Query(value = "select t from Tag t where t.project.id = ?1")
    List<Tag> findTags(Integer projectId);

    @Query(value = "select r from Report r where r.project.id = ?1")
    List<Report> findReports(Integer projectId);

}
