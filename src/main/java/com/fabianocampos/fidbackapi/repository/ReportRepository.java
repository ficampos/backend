package com.fabianocampos.fidbackapi.repository;

import com.fabianocampos.fidbackapi.domain.CommentReport;
import com.fabianocampos.fidbackapi.domain.Project;
import com.fabianocampos.fidbackapi.domain.Report;
import com.fabianocampos.fidbackapi.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {

    List<Report> findByUserId(Integer userId);

    @Query(value = "select cr from CommentReport cr where cr.report.id = ?1 order by cr.createdAt")
    List<CommentReport> findComments(Integer reportId);

    @Query(value = "select r.users from Report r where r.id = ?1")
    List<User> findParticipants(Integer reportId);

    @Query(value = "select r.user from Report r where r.id = ?1")
    User findOwner(Integer reportId);

}
