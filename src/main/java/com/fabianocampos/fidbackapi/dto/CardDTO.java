package com.fabianocampos.fidbackapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Integer timeSpent;

    private Integer reportId;

    private Integer labelId;
}
