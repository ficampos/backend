package com.fabianocampos.fidbackapi.dto.converter;

import com.fabianocampos.fidbackapi.domain.User;
import com.fabianocampos.fidbackapi.dto.Converter;
import com.fabianocampos.fidbackapi.dto.ParticipantDTO;
import com.fabianocampos.fidbackapi.dto.UserDTO;
import com.fabianocampos.fidbackapi.dto.converter.enums.Operation;
import com.fabianocampos.fidbackapi.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class ParticipantConverter implements Converter<User, ParticipantDTO> {

    @Autowired
    private UserService userService;

    @Override
    public ParticipantDTO encode(User user) {
        return ParticipantDTO.builder().id(user.getId()).firstName(user.getFirstName()).lastName(user.getLastName()).nickname(user.getNickname()).build();
    }

    @Override
    public User decode(ParticipantDTO participantDTO, Operation operation) {
        User user = null;
        if (operation == Operation.FIND) {
            user = userService.findById(participantDTO.getId());
        }
        return user;
    }

}
